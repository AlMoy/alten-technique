# API Rest - test technique
Cette API Rest a été montée avec Spring, en utilisant Gradle.\
Ci-joint les commandes `curl`, sous Unix, pour communiquer avec l'API Rest.\
Si on souhaite afficher tous les produits :
```bash
curl http://localhost:3000/products -X GET \
     -H "Content-Type: application/json"
```
Si on souhaite afficher le produit ayant pour id `1` :
```bash
curl http://localhost:3000/products/1 -X GET \
     -H "Content-Type: application/json"
```
Si on souhaite créer un nouveau produit :
```bash
curl http://localhost:3000/products -X POST \
     -H "Content-Type: application/json" \
     -d '{ "code": "f230fh0g3", "name": "Bamboo Watch", "description": "Product Description", "image": "bamboo-watch.jpg", "price": 65, "category": "Accessories", "quantity": 24, "inventoryStatus": "INSTOCK", "rating": 5 }'
```
Si on souhaite modifier le produit ayant pour id `9` :
```bash
curl http://localhost:3000/products/9 -X PATCH \
     -H "Content-Type: application/json" \
     -d '{ "name": "White Watch", "image": "bamboo-watch.jpg", "rating": 5 }'
```
Si on souhaite supprimer le produit ayant pour id `9` :
```bash
curl http://localhost:3000/products/9 -X DELETE \
     -H "Content-Type: application/json"
```