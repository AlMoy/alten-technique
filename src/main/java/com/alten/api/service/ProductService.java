package com.alten.api.service;

import com.alten.api.model.Product;
import com.alten.api.repository.ProductRepository;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Data
@Service
public class ProductService {
    @Autowired
    private ProductRepository productRepository;

    public Iterable<Product> getAll() {
        return this.productRepository.findAll();
    }

    public Optional<Product> getById(final Long id) {
        return this.productRepository.findById(id);
    }

    public Product save(Product product) {
        return this.productRepository.save(product);
    }

    public void delete(final Long id) {
        this.productRepository.deleteById(id);
    }
}
