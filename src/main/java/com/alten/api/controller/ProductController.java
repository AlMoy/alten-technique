package com.alten.api.controller;

import com.alten.api.model.Product;
import com.alten.api.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
public class ProductController {
    @Autowired
    private ProductService productService;

    @GetMapping("/products")
    public Iterable<Product> getProducts() {
        return this.productService.getAll();
    }

    @GetMapping("/products/{id}")
    public Product getProduct(@PathVariable("id") final Long id) {
        return this.productService.getById(id).orElse(null);
    }

    @PostMapping("/products")
    public Product postProduct(@RequestBody Product requestBody) {
        return this.productService.save(requestBody);
    }

    @PatchMapping("/products/{id}")
    public Product patchProduct(@PathVariable("id") final Long id, @RequestBody Product requestBody) {
        Optional<Product> optionalProduct = this.productService.getById(id);

        if (optionalProduct.isEmpty()) {
            return null;
        }

        Product product = optionalProduct.get();

        String code = requestBody.getCode();
        if (code != null)
            product.setCode(code);

        String name = requestBody.getName();
        if (name != null)
            product.setName(name);

        String description = requestBody.getDescription();
        if (description != null)
            product.setDescription(description);

        Float price = requestBody.getPrice();
        if (price != null)
            product.setPrice(price);

        Integer quantity = requestBody.getQuantity();
        if (quantity != null)
            product.setQuantity(quantity);

        String inventoryStatus = requestBody.getInventoryStatus();
        if (inventoryStatus != null)
            product.setInventoryStatus(inventoryStatus);

        String category = requestBody.getCategory();
        if (category != null)
            product.setCategory(category);

        product.setImage(requestBody.getImage());
        product.setRating(requestBody.getRating());

        return this.productService.save(product);
    }

    @DeleteMapping("/products/{id}")
    public void deleteProduct(@PathVariable("id") final Long id) {
        this.productService.delete(id);
    }
}
