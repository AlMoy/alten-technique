package com.alten.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AltenTechniqueApplication {

	public static void main(String[] args) {
		SpringApplication.run(AltenTechniqueApplication.class, args);
	}

}
