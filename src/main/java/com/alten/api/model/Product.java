package com.alten.api.model;

import jakarta.persistence.*;
import lombok.Data;

@Data
@Entity
@Table(name = "Product")
public class Product {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String code;

    private String name;

    private String description;

    private Float price;

    private Integer quantity;

    private String inventoryStatus;

    private String category;

    private String image;

    private Float rating;
}
