drop table if exists Product;

create table Product (
    id int auto_increment primary key,
    code varchar(250) not null,
    name varchar(250) not null,
    description text not null,
    price float not null,
    quantity int not null,
    inventoryStatus varchar(250) not null,
    image varchar(250),
    rating float
);

insert into Product (code, name, description, price, quantity, inventoryStatus, image, rating) values
    ('P1001', 'Smartphone XYZ', 'Un smartphone haut de gamme avec écran OLED de 6 pouces.', 899.99, 50, 'En stock', 'smartphone_xyz.jpg', 4.5),
    ('P1002', 'Ordinateur portable ABC', 'Un ordinateur portable puissant avec processeur Intel Core i7.', 1499.99, 30, 'En stock', 'laptop_abc.jpg', 4.7),
    ('P1003', 'Casque audio Bluetooth', 'Casque audio sans fil avec suppression active du bruit.', 199.99, 100, 'En stock', 'casque_audio.jpg', 4.2),
    ('P1004', 'Tablette Android', 'Tablette Android 10 pouces avec caméra HD et 64 Go de stockage.', 299.99, 20, 'En stock', 'tablette_android.jpg', 4.0),
    ('P1005', 'Écouteurs sans fil', 'Écouteurs intra-auriculaires sans fil avec boîtier de charge.', 79.99, 150, 'En stock', 'ecouteurs_sans_fil.jpg', 4.3),
    ('P1006', 'Téléviseur 4K', 'Téléviseur 4K de 55 pouces avec Smart TV intégré.', 799.99, 10, 'En stock', 'televiseur_4k.jpg', 4.8),
    ('P1007', 'Enceinte Bluetooth', 'Enceinte portable Bluetooth étanche avec son stéréo.', 59.99, 80, 'En stock', 'enceinte_bluetooth.jpg', 4.1);